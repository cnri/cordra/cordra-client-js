tsc
sed -i.bak -E -e 's#^(import.*)$#//\1#' dist/unbundled/dom-equivalents/fetch/browser.js
find dist/unbundled -type f | xargs sed -i.bak -E -e "/^..port / s#/dom-equivalents/([^/'\"]*)/?#/dom-equivalents/\1/browser#"
find dist/unbundled -type f | xargs sed -i.bak -E -e "/^..port / s#(\\ from\\ ['\"][^'\"]*)(['\"])#\\1.js\\2#"
find dist/unbundled -name '*.bak' -delete
find dist/unbundled/dom-equivalents -type f -not -name browser.js -delete
