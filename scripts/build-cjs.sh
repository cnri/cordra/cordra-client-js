babel --config-file ./babel.config.node.cjs.json src --extensions .ts --out-dir dist/cjs --source-maps && \
babel --config-file ./babel.config.node.cjs.json node_modules/node-fetch --extensions .js --out-dir dist/cjs/node_modules/node-fetch --source-maps --copy-files && \
babel --config-file ./babel.config.node.cjs.json node_modules/data-uri-to-buffer --extensions .js --out-dir dist/cjs/node_modules/data-uri-to-buffer --source-maps --copy-files && \
babel --config-file ./babel.config.node.cjs.json node_modules/fetch-blob --extensions .js,.cjs --keep-file-extension --out-dir dist/cjs/node_modules/fetch-blob --source-maps --copy-files && \
babel --config-file ./babel.config.node.cjs.json node_modules/formdata-polyfill --extensions .js --out-dir dist/cjs/node_modules/formdata-polyfill --source-maps --copy-files && \
cp scripts/commonjs-package.json dist/cjs/package.json && \
sed -i.bak -E -e 's#"type": "module"#"type": "commonjs"#' dist/cjs/node_modules/*/package.json
