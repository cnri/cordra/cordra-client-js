/* eslint-disable import/no-extraneous-dependencies */
import * as terser from 'rollup-plugin-terser';
import * as babel from '@rollup/plugin-babel';
import * as nodeResolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import * as rollup from 'rollup';
// Prefer this once IDEs stop complaining:
//import pkg from './package.json' assert {type: "json"};
import { createRequire } from 'node:module';

const require = createRequire(import.meta.url);
const pkg = require('./package.json');

function versionedFilename(filename) {
    return filename.replace(/(\.[^/]*)?\.js$/, '-' + pkg.version + '$1.js');
}

function minFilename(filename) {
    return filename.replace(/(\.[^/]*)?\.js$/, '$1.min.js');
}

function versionedMinFilename(filename) {
    return filename.replace(/(\.[^/]*)?\.js$/, '-' + pkg.version + '$1.min.js');
}

function inputOptions(esm, min) {
    const babelOptions = {
        exclude: ["dist/**", "node_modules/**"],
        extensions: [".ts", "tsx"],
        babelHelpers: 'bundled'
    };
    if (esm) babelOptions.targets = "supports es6-module, not dead";
    const res = {
        input: "./src/index.ts",
        plugins: [
            babel.babel(babelOptions),
            nodeResolve.nodeResolve({
                browser: true,
                extensions: [ ".ts", ".mjs", ".js", ".json", ".node" ]
            }),
            commonjs({
                include: "node_modules/**",
                // if false then skip sourceMap generation for CommonJS modules
                sourceMap: false // Default: true
            })
        ]
    };
    if (min) res.plugins.push(terser.terser());
    return res;
}

const mainOptions = { file: pkg.exports.browser.require, format: 'umd', name: 'cnri', sourcemap: true };
const moduleOptions = { file: pkg.exports.browser.import, format: 'esm', sourcemap: true };

async function build() {
    const bundle = await rollup.rollup(inputOptions(false, false));
    await bundle.write(mainOptions);
    await bundle.write({ ...mainOptions, file: versionedFilename(mainOptions.file) });
    const minBundle = await rollup.rollup(inputOptions(false, true));
    await minBundle.write({ ...mainOptions, file: minFilename(mainOptions.file) });
    await minBundle.write({ ...mainOptions, file: versionedMinFilename(mainOptions.file) });
    const esmBundle = await rollup.rollup(inputOptions(true, false));
    await esmBundle.write(moduleOptions);
    await esmBundle.write({ ...moduleOptions, file: versionedFilename(moduleOptions.file) });
    const esmMinBundle = await rollup.rollup(inputOptions(true, true));
    await esmMinBundle.write({ ...moduleOptions, file: minFilename(moduleOptions.file) });
    await esmMinBundle.write({ ...moduleOptions, file: versionedMinFilename(moduleOptions.file) });
}

await build();
