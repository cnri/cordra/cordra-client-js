import crypto from 'crypto';
// eslint-disable-next-line import/no-extraneous-dependencies
import jwkToPem from 'jwk-to-pem';
import { JsonWebKey } from '../../client/Interfaces';

export default {
    subtle: {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        importKey(format: 'jwk', keyData: JsonWebKey, algo: unknown, extractable: boolean, keyUsages: unknown[]): Promise<string> {
            if (keyData.kty !== 'RSA' || !keyData.n || !keyData.e) throw new Error('Unsupported private key');
            const pem = jwkToPem(keyData as { kty: 'RSA'; n: string; e:string }, { private: true });
            return Promise.resolve(pem);
        },
        sign(algo: string, key: string, data: Uint8Array): Promise<ArrayBuffer> {
            const sign = crypto.createSign('SHA256');
            sign.update(data);
            const buffer = sign.sign(key);
            return Promise.resolve(buffer.buffer);
        }
    },
    getRandomValues(array: Uint8Array): Uint8Array {
        const buf = crypto.randomBytes(array.length);
        array.set(buf);
        return array;
    }
};
