// eslint-disable-next-line import/no-extraneous-dependencies
import 'whatwg-fetch';

export default fetch;
export const Headers = window.Headers;
export const Request = window.Request;
export const Blob = window.Blob;
export const FormData = window.FormData;
